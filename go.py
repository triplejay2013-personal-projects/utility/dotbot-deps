"""Go package handler."""

import shutil
import sys
from typing import Dict
from pathlib import Path

import dotbot
import yaml
from subprocess import CalledProcessError, TimeoutExpired
from common.fileop import load_file
from common.shell import Shell

if sys.version_info[0] < 3:
    raise Exception("Plugin only supports python3 not Python%s" % sys.version_info[0])


class Go(dotbot.Plugin):

    _directive = "go"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        defaults = self._context.defaults().get("outputs", {})
        self.stdout = defaults.get("stdout", False)
        self.stderr = defaults.get("stderr", False)

        # Use low-info for shell commands
        self.shell = Shell(stdout=self.stdout, stderr=self.stderr, logger=self._log, loginfo=self._log.lowinfo)

    def can_handle(self, directive: str) -> bool:
        return directive == self._directive

    def load_packages(self, data: Dict):
        """Config file can have the following options.

        version (optional) - May specify version to autoreplace in remote (will replace instances of $VERSION)
        vars (optional) - go vars to execute with command
        cmd (required) - go command to execute
        check (optional) - defaults to False. Specify if executable should be checked first
        """
        filename = data["file"]
        path = load_file(filename)
        with open(path) as f:
            config = yaml.safe_load(f)
        if not config:
            # Nothing was found, nothing to parse
            return {}
        packages = {}
        for k, v in config.items():
            tmp = {}
            tmp["check"] = v.get("check", True)
            tmp["version"] = v.get("version", "")
            tmp["vars"] = {}
            goenv = self.shell.run("go env")[0].split("\n")
            godict = {}
            for env in goenv:
                key, val = env.split("=", 1)
                godict.update({key: val.strip('"')})
            tmp["vars"].update(godict)
            for var in v.get("vars"):
                tmp["vars"].update(var)
            tmp["vars"].update({"HOME": str(Path.home())})
            tmp["cmd"] = v.get("cmd").replace("$VERSION", tmp["version"])
            gobin = "/usr/local/go/bin/go "
            tmp["cmd"] = gobin + tmp["cmd"] if not tmp["cmd"].startswith("go") else tmp["cmd"].replace("go ", gobin)
            packages.update({k: tmp})
        return packages

    def handle(self, directive: str, data: Dict) -> bool:
        if directive != self._directive:
            raise ValueError("Apt cannot handle directive %s" % directive)

        success = True

        for k, v in self.load_packages(data).items():
            try:
                # Check to see if already installed
                if shutil.which(k) is not None:
                    continue  # cmd already installed, skip
                self.shell.run(v["cmd"], msg=f"Installing {k}", env=v["vars"])
            except (CalledProcessError, TimeoutExpired, ValueError) as e:
                self._log.error("Error installing %s: %s" % (k, e))
                success = False

        if success:
            self._log.info("%s packages installed successfully" % directive.capitalize())
        else:
            self._log.warn("Some packages failed to install")

        return success
