# dotbot-deps

Combines dotbot plugins for 'apt' and 'pip' packages (and maybe more). Consolidates and improves on the design. (WIP ;))

# Acknowledgments

Derived from apt and pip plugins listed [here](https://github.com/anishathalye/dotbot/wiki/Plugins)

- [apt](https://github.com/bryant1410/dotbot-apt)
- [pip](https://github.com/sobolevn/dotbot-pip)

Images:
* <a target="_blank" href="undefined/icons/set/bot">Bot icon</a> icon by <a target="_blank" href="">Icons8</a>


# Apt packages

apt install packages (Expects distro supporting apt)
* TODO this could easily be updated to support other distros as well (add bin option)
* Versioning not supported yet

## Example

Options:
```bash
...
- apt:
    file: config/packages.conf
    upgrade: true
    check: true
...
```

* `check`: Look to see if package exists

Config file:
```text
pckg1
# Comment
pckg2
pckg3
```

# Pip packages

pip install packages

## Example

Options:
```bash
...
- pip:
    file: config/pip-requirements.txt
    binary: /usr/local/bin/pip3
    user: false
    check: true
...
```

* `check`: Look to see if package exists


Config file:
* Follows the convention of pip req files
```text
# comment
req==3.6.1
req1>=3.2
req3
```

# Web packages

Install packages from alternate sources (wget, curl etc.)

## Example

Options:
```bash
...
- web:
    file: config/web-packages.yaml
    check: true
...
```

Config file:
```text
jq:
  bin: wget
  version: jq-1.6
  remote: https://github.com/stedolan/jq/releases/download/$VERSION/jq-linux64

sops:
  version: 3.2.0
  remote: https://github.com/mozilla/sops/releases/download/$VERSION/sops-$VERSION.linux
  flags: -LO

telepresence:
  remote: https://packagecloud.io/install/repositories/datawireio/telepresence/script.deb.sh
  local-file: telepresence
  alt-install:
    cmd: sudo apt install --no-install-recommends -y telepresence
    is_script: True
  flags: -s

terraform:
  version: 0.13.5
  remote: https://releases.hashicorp.com/terraform/$VERSION/terraform_$VERSION_linux_amd64.zip
  bin: wget
  archived:
    cmd: unzip
```

* For a full list of options check out `handlers/web.py`
