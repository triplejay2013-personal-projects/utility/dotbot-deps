"""Pip package handler."""
import os
import sys
import tempfile
from dataclasses import dataclass
from pathlib import Path
from subprocess import CalledProcessError, TimeoutExpired
from typing import Dict

import dotbot
import yaml
from common.fileop import expand_path, load_file, pushd
from common.shell import Shell

if sys.version_info[0] < 3:
    raise Exception("Plugin only supports python3 not Python%s" % sys.version_info[0])


@dataclass
class Package:
    """Loads yaml config into memory.

    Attributes:
        name (str): The name of the package we are retrieving
        remote (str): Where to retrieve web package
        version (str): Used for string interpolation. Use $VERSION as a placeholder
        filename (str): Alternate name for downloaded file
        bin_cmd (str): The binary command to execute to retrieve remote packages
        path (str): Where to place downloaded package
        flags (str): flags to pass to binary (specify str with -f for short flags and --flag for long flags)
        archive (str): Instructions to unpack downloaded package
        static (bool): File is a static file (as opposed to executable script)
        pipe (str): Pipe downloaded package straight into piped command
        use_pipe (bool): Set to true if `pipe` is defined. Otherwise uses default pipe
        alt (str): Instructions for alternate installation process
        extras (str): Post download instructions
    """

    name: str
    remote: str
    version: str = None
    filename: str = None
    bin_cmd: str = "curl"
    path: str = None
    flags: str = None
    archive: str = None
    static: bool = False
    pipe: str = None
    use_pipe: bool = False
    alt: str = None
    extras: str = None

    def __post_init__(self):
        """Extra customization of our configuration."""
        PATH = "/usr/local/bin/%s"

        self.remote = self.remote.replace("$VERSION", self.version) if self.version else self.remote
        self.filename = self.filename if self.filename else self.remote.split("/")[-1]
        self.path = expand_path(self.path if self.path else PATH % self.name)
        self.archive = self.archive.replace("$LOCAL_FILE", self.filename) if self.archive else self.archive
        self.use_pipe = self.use_pipe if self.use_pipe else self.pipe is not None
        self.pipe = "sudo bash" if self.use_pipe and self.pipe is None else self.pipe
        if self.flags is None:
            self.flags = "-LO" if self.bin_cmd == "curl" else ""

        # Some flags don't make sense when used together. Give users errors when config is not setup properly.
        expect_alone = ["alt", "archive", "static"]
        for e in expect_alone:
            attr = self.__getattribute__(e)
            for e_ in expect_alone:
                if e == e_:
                    continue  # Don't compare to yourself
                attr_ = self.__getattribute__(e_)
                if attr and attr_:
                    raise ValueError(f"Cannot use `{e}` and `{e_}` together")

    def download(self, shell: Shell):
        """How we will download and manipulate this package.

        Parameters:
            shell (Shell): How are we going to talk to the shell from python
        """
        cmd = f"{self.bin_cmd} {self.flags} {self.remote}"
        if self.use_pipe:
            shell.run(f"{cmd} | {self.pipe}", msg=f"Downloading {self.name} then sending to pipe: {self.pipe}")
            return  # We sent the downloaded file straight to another command, no more file manipulation is needed

        shell.run(cmd, msg=f"Downloading {self.name}")
        if self.alt:
            shell.run(self.alt, msg=f"Alternate install {self.name}")
        else:
            if self.filename not in os.listdir():
                # Try to rename the downloaded file to local filename
                os.rename(self.remote.split("/")[-1], self.filename)

            if self.archive:
                shell.run(self.archive, msg=f"Unpacking Archive `{self.filename}` using cmd `{self.archive}`")
            elif not self.static:
                shell.run(["chmod", "+x", self.filename], msg=f"Updating permissions for {self.name}")
            shell.run(["sudo", "mv", self.filename, self.path], msg=f"Moving {self.name} to {self.path}")
            # NOTE: Using '[]' for shell.run helps when expanding `filename`, which MAY contain spaces
        if self.extras:
            shell.run(self.extras, msg="Running Post Installation setup")

    def __get_keys(self):
        """Retrieve defined keys from dataclass."""
        keys = [k for k in self.__dict__.keys() if self.__getattribute__(k) and k != "package"]
        ret = []
        for k in keys:
            if k == "package":
                continue
            if k == "pipe" and not self.__getattribute__("use_pipe"):
                continue
            ret.append(k)
        return ret

    def __repr__(self):
        """Show package with its defined attributes."""
        return self.__class__.__name__ + "(" + f"package={self.name}, config=" + str(self.__get_keys()) + ")"


class Web(dotbot.Plugin):

    _directive = "web"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        defaults = self._context.defaults().get("outputs", {})
        self.stdout = defaults.get("stdout", False)
        self.stderr = defaults.get("stderr", False)

        # Use low-info for shell commands
        self.shell = Shell(stdout=self.stdout, stderr=self.stderr, logger=self._log, loginfo=self._log.lowinfo)

    def can_handle(self, directive: str) -> bool:
        return directive == self._directive

    def load_packages(self, data: Dict):
        with open(load_file(data["file"])) as f:
            config = yaml.safe_load(f)
        if not config:
            return  # Nothing was found, nothing to parse
        packages = []
        for k, v in config.items():
            packages.append(Package(name=k, **v))
        return packages

    def handle(self, directive: str, data: Dict) -> bool:
        if directive != self._directive:
            raise ValueError("Apt cannot handle directive %s" % directive)

        success = True
        packages = self.load_packages(data)
        tempdir = tempfile.mkdtemp()
        self._log.info("Loading packages in dir: `%s`" % tempdir)
        if packages:
            with pushd(tempdir):
                for package in packages:
                    try:
                        package.download(self.shell)
                    except (CalledProcessError, TimeoutExpired, ValueError) as e:
                        self._log.error("Error installing %s: %s" % (package.name, e))
                        success = False
                    finally:
                        # Clean up left over files
                        p = Path(package.filename)
                        if p.exists():
                            p.unlink()
        else:
            success = False  # Nothing was loaded / handled

        if success:
            self._log.info("%s packages installed successfully" % directive.capitalize())
        else:
            self._log.warn("Some packages failed to install")

        return success
