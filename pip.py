"""Pip package handler."""
import re
import shutil
import sys
from pathlib import Path
from subprocess import CalledProcessError, TimeoutExpired
from typing import Dict

import dotbot
from common.fileop import expand_path, load_file
from common.shell import Shell

if sys.version_info[0] < 3:
    raise Exception("Plugin only supports python3 not Python%s" % sys.version_info[0])


class Pip(dotbot.Plugin):

    _directive = "pip"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        defaults = self._context.defaults().get("outputs", {})
        self.stdout = defaults.get("stdout", False)
        self.stderr = defaults.get("stderr", False)

        # Use low-info for shell commands
        self.shell = Shell(stdout=self.stdout, stderr=self.stderr, logger=self._log, loginfo=self._log.lowinfo)

    def can_handle(self, directive: str) -> bool:
        return directive == self._directive

    def get_requirements(self, data: Dict):
        filename = data["file"]
        # search and expand values if found
        thefile = load_file(filename)
        with open(thefile) as f:
            reqs = {}
            for line in f.readlines():
                if line.startswith("#") or line.startswith("\n"):
                    continue
                info = [s for s in re.split("[=<>]", line) if s]
                package, version = None, None
                if len(info) == 2:
                    package = info[0].strip("\n")
                    version = info[1].strip("\n")
                elif len(info) == 1:
                    # Expand (non-versioned) paths
                    package = expand_path(info[0].strip("\n"))
                else:
                    raise RuntimeError("unexpected split occured when parsing requirements.")

                reqs.update({package: version})

        return reqs

    def handle(self, directive: str, data: Dict) -> bool:
        if directive != self._directive:
            raise ValueError("Apt cannot handle directive %s" % directive)

        success = True
        requirements = self.get_requirements(data)

        binary = data.get("binary", "pip")
        param = "--user" if data.get("user") else ""
        force = "--force-reinstall" if data.get("force") else ""

        for req, ver in requirements.items():
            try:
                if data.get("check"):
                    if Path(req).exists():
                        # If we are a path, check that we exist
                        continue
                    elif shutil.which(req) is not None:
                        continue  # cmd already installed, skip
                command = f"{binary} install {force} {req}=={ver}" if ver else f"{binary} install {force} {req}"
                msg = ""
                if not param:
                    # Must have sudo privileges if not doing user install
                    command = "sudo " + command
                    msg = f"Installing {req} with sudo privelages"
                else:
                    command = command + param
                    msg = "Installing {req} for user"
                self.shell.run(command, msg=msg)
            except (CalledProcessError, TimeoutExpired, ValueError) as e:
                self._log.error(e)
                success = False

        if success:
            self._log.info("%s packages installed successfully" % directive.capitalize())
        else:
            self._log.warn("Some packages failed to install")

        return success
