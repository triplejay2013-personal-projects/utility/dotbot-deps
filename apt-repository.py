"""Apt package handler."""
import re
import sys
from subprocess import CalledProcessError, TimeoutExpired
from typing import Dict

import dotbot
from common.fileop import load_file
from common.shell import Shell

REGEX = re.compile(r"\$\(([\w\s-]+)\)")

if sys.version_info[0] < 3:
    raise Exception("Plugin only supports python3 not Python%s" % sys.version_info[0])


class Apt(dotbot.Plugin):
    """Install apt packages (using sudo)."""

    _directive = "apt-repository"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        defaults = self._context.defaults().get("outputs", {})
        self.stdout = defaults.get("stdout", False)
        self.stderr = defaults.get("stderr", False)

        # Use low-info for shell commands

        self.shell = Shell(
            stdout=self.stdout,
            stderr=self.stderr,
            logger=self._log,
            loginfo=self._log.lowinfo,
        )

    def can_handle(self, directive: str) -> bool:
        return directive == self._directive

    def handle(self, directive: str, data: Dict) -> bool:
        success = True

        if directive != self._directive:
            raise ValueError("Apt cannot handle directive %s" % directive)

        with open(load_file(data["file"])) as f:
            repositories = [r for r in f.read().splitlines() if r and not r.startswith("#")]
        if repositories:
            try:
                for repo in repositories:
                    # Discover subshell interpolations and evaluate them
                    # NOTE: This might be a good thing to move to common.shell in the future?
                    subshell_interpolations = REGEX.findall(repo)
                    for s in subshell_interpolations:
                        out, err = self.shell.run(s)
                        repo = REGEX.sub(out, repo, count=1)
                    self.shell.run(["sudo", "apt-add-repository", repo], msg=f"Adding repository {repo}")
            except (CalledProcessError, TimeoutExpired) as e:
                self._log.error(e)
                return False
        else:
            success = False

        if success:
            self._log.info("%s packages installed successfully" % directive.capitalize())
        else:
            self._log.warn("Some packages failed to install")

        return success
