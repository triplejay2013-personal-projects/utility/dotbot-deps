"""Apt package handler."""
import shutil
import sys
from subprocess import CalledProcessError, TimeoutExpired
from typing import Dict, List

import dotbot
from common.fileop import load_file
from common.shell import Shell

if sys.version_info[0] < 3:
    raise Exception("Plugin only supports python3 not Python%s" % sys.version_info[0])


class Apt(dotbot.Plugin):
    """Install apt packages (using sudo)."""

    _directive = "apt"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        defaults = self._context.defaults().get("outputs", {})
        self.stdout = defaults.get("stdout", False)
        self.stderr = defaults.get("stderr", False)

        # Use low-info for shell commands
        self.shell = Shell(
            stdout=self.stdout,
            stderr=self.stderr,
            logger=self._log,
            loginfo=self._log.lowinfo,
        )

    def can_handle(self, directive: str) -> bool:
        return directive == self._directive

    def load_packages(self, data: Dict) -> List:
        filename = data["file"]
        path = load_file(filename)
        with open(path) as f:
            # TODO handle versioning info (if provided)
            packages = [p for p in f.read().splitlines() if p and not p.startswith("#")]
        return packages

    def handle(self, directive: str, data: Dict) -> bool:
        if directive != self._directive:
            raise ValueError("Apt cannot handle directive %s" % directive)

        success = True

        try:
            if data.get("update"):
                self._log.lowinfo("Updating Apt")
                with self.shell.activate_stdout():
                    self.shell.run("sudo apt update -y")
            if data.get("upgrade"):
                self._log.lowinfo("Upgrading Apt")
                with self.shell.activate_stdout():
                    self.shell.run("sudo apt upgrade -y")
            if data.get("autoremove"):
                self._log.lowinfo("Autoremoving Apt")
                with self.shell.activate_stdout():
                    self.shell.run("sudo apt autoremove -y")
        except (CalledProcessError, TimeoutExpired) as e:
            self._log.error(e)
            return False
        if data.get("install"):
            packages = self.load_packages(data)
            for package in packages:
                try:
                    if data.get("check"):
                        if shutil.which(package) is not None:
                            continue  # cmd already installed, skip
                    self.shell.run(
                        f"sudo apt install -y {package}",
                        msg=f"Installing apt package {package}",
                    )
                except (CalledProcessError, TimeoutExpired) as e:
                    self._log.error(e)
                    success = False

        if success:
            self._log.info(
                "%s packages installed successfully" % directive.capitalize()
            )
        else:
            self._log.warn("Some packages failed to install")

        return success
